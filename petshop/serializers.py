from rest_framework import serializers

from .models import Agendamento


class AgendamentoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Agendamento
        # fields = ('nome_dono', 'endereco', 'telefone', 'nome_animal', 'porte', 'tipo_tosa', 'observacao', 'data', 'horario')
        exclude = []