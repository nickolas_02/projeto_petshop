# Generated by Django 3.0.6 on 2020-05-25 21:10

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('petshop', '0015_auto_20200525_2105'),
    ]

    operations = [
        migrations.AlterField(
            model_name='agendamento',
            name='dt_agendamento',
            field=models.DateField(),
        ),
    ]
