# Generated by Django 3.0.6 on 2020-06-16 00:40

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('petshop', '0027_auto_20200613_2148'),
    ]

    operations = [
        migrations.AlterField(
            model_name='agendamento',
            name='dt_agendamento',
            field=models.CharField(max_length=50),
        ),
    ]
