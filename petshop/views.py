from django.shortcuts import render, redirect
from django.http import JsonResponse, HttpResponse
from django.views.generic import TemplateView, DetailView
from .models import Agendamento, Cliente
from django.contrib import auth
from urllib.error import HTTPError
import json

import pyrebase
import datetime

# Create your views here.

###### CONFIGURACAO FIREBASE
config = {
  'apiKey': "AIzaSyA8ifHYX8gYmc2SC4o-4M8KkG01I6s0wzU",
  'authDomain': "auaushow-d8127.firebaseapp.com",
  'databaseURL': "https://auaushow-d8127.firebaseio.com",
  'projectId': "auaushow-d8127",
  'storageBucket': "auaushow-d8127.appspot.com",
  'messagingSenderId': "419191598652",
  'appId': "1:419191598652:web:0378efd9fbb8c8c28d5d39",
  'measurementId': "G-WD6GJ04R13"
}


firebase = pyrebase.initialize_app(config)
authe = firebase.auth()
# database = firebase.database()


def logout(req):
    auth.logout(req)
    return redirect('login')


def login(req):
    if req.method == "GET":
        return render(req, 'login.html')

    if req.method == "POST":
        email = req.POST.get("email")
        senha = req.POST.get("senha")
        try:
            user = authe.sign_in_with_email_and_password(email, senha)
        except:
            context = {
                'erro': {
                    'title': "Erro ao Logar",
                    'msg': "Verifique seu e-mail ou senha."
                }
            }
            return render(req, 'login.html', context)
        session_id = user['localId']
        req.session['uid'] = str(session_id)
        return redirect('index')


# criar login no firebase
def registrar(req):
    email = req.POST.get("email")
    senha = req.POST.get("senha")

    try:
        authe.create_user_with_email_and_password(email, senha)
    except:
        context = {
            'erro': {
                'title': "Erro se cadastrar",
                'msg': "Verifique o e-mail."
            }
        }
        return render(req, 'login.html', context)
    user = authe.sign_in_with_email_and_password(email, senha)
    session_id = user['localId']
    req.session['uid'] = str(session_id)

    return redirect('cadastro')


# cadastrar usuario no banco de dados
def cadastro(req):
    if req.method == 'GET':
        try:
            uid = req.session['uid']
        except:
            return redirect('login')
        context = {'logado': True}
        return render(req, "cadastro.html", context)

    if req.method == 'POST':
        # tenta receber informacoes enviadas no post
        try:
            nome = req.POST['nome']
            telefone = req.POST['telefone']
            dt_nascimento = req.POST['dt_nascimento']
            cpf = req.POST['cpf']
            sexo = req.POST['sexo']
            rua = req.POST['rua']
            numero = req.POST['numero']
            complemento = req.POST['complemento']
            bairro = req.POST['bairro']
            cidade = req.POST['cidade']
            cep = req.POST['cep']
            uid = req.session['uid']

            # tenta salvar dados no banco de dados
            cliente = Cliente(nome=nome, telefone=telefone, dt_nascimento=dt_nascimento, cpf=cpf,
                sexo=sexo, rua=rua, numero=numero, complemento=complemento, bairro=bairro,
                cidade=cidade, cep=cep, uid=uid)

            cliente.save()

        # caso de erro, volta para a tela de cadastro com o status de erro true
        except:
            context = {
                'logado': True,
                'erro': True
            }

            return render(req, 'cadastro.html', context)
        return redirect('index')


# api para fazer cadastro do cliente
def apiCadastro(req):
    if req.method == 'POST':
        # recebe a requisicao em bytes e decodifica em string
        data_bytes = req.body.decode('utf8').replace("'", '"')

        # converte a string em dicionario para obter os dados
        data_dict = json.loads(data_bytes)

        try:
            nome = data_dict["nome"]
            telefone = data_dict["telefone"]
            dt_nascimento = data_dict["dt_nascimento"]
            cpf = data_dict["cpf"]
            sexo = data_dict["sexo"]
            rua = data_dict["rua"]
            numero = data_dict["numero"]
            complemento = data_dict["complemento"]
            bairro = data_dict["bairro"]
            cidade = data_dict["cidade"]
            cep = data_dict["cep"]
            uid = data_dict["uid"]
        except:
            retorno = []
            retorno.append({"status": "403", "msg": "informacoes invalidas"})
            return HttpResponse(json.dumps(retorno))

        try:
            cliente = Cliente(nome=nome, telefone=telefone, dt_nascimento=dt_nascimento, cpf=cpf,
                sexo=sexo, rua=rua, numero=numero, complemento=complemento, bairro=bairro,
                cidade=cidade, cep=cep, uid=uid)

            cliente.save()

        except:
            Cliente.objects.filter(uid=uid).update(
                nome=nome,
                telefone=telefone,
                dt_nascimento=dt_nascimento,
                cpf=cpf,
                sexo=sexo,
                rua=rua,
                numero=numero,
                complemento=complemento,
                bairro=bairro,
                cidade=cidade,
                cep=cep
            )
            retorno = []
            retorno.append({"status": "201", "msg": "cadastro atualizado"})
            return HttpResponse(json.dumps(retorno))

        retorno = []
        retorno.append({"status": "201", "msg": "usuario cadastrado"})

        return HttpResponse(json.dumps(retorno))


# retorna dados do cadastro do cliente no site
def meuPerfil(req):
    if req.method == 'GET':

        # verifica se esta logado
        try:
            uid = req.session['uid']
        except:
            return render(req, 'index.html')

        # verifica se o usuario ja esta cadastrado e retorna as informacoes cadastradas
        try:
            userInfo = Cliente.objects.filter(uid=uid).values()[0]
        except:
            return redirect('cadastro')
        context = {
            'logado': True,
            'userInfo': userInfo
        }
        return render(req, 'meu_perfil.html', context)

    if req.method == 'POST':
        try:
            Cliente.objects.filter(uid=req.session['uid']).update(
                nome=req.POST['nome'],
                telefone=req.POST['telefone'],
                dt_nascimento=req.POST['dt_nascimento'],
                cpf=req.POST['cpf'],
                sexo=req.POST['sexo'],
                rua=req.POST['rua'],
                numero=req.POST['numero'],
                complemento=req.POST['complemento'],
                bairro=req.POST['bairro'],
                cidade=req.POST['cidade'],
                cep=req.POST['cep']
            )
        except:
            context = {
                'logado': True,
                'erro': True
            }
            return render(req, 'meu_perfil.html', context)

        return redirect('index')


# retorna a pagina inicial no site
def homePageView(req):
    try:
        uid = req.session['uid']
    except:
        return render(req, 'index.html')
    context = {
        'logado': True,
        # 'erro': False
    }
    return render(req, 'index.html', context)


# retorna a pagina Quem Somos no site
def quemSomos(req):
    try:
        uid = req.session['uid']
    except:
        context = {
            'logado': False,
        }
        return render(req, 'quem-somos.html', context)
    context = {
        'logado': True,
    }
    return render(req, 'quem-somos.html', context)


# retorna os agendamentos do cliente
def agendamentos(req):
    # verifica se o usuario esta logado, caso nao, o redireciona para a tela de login
    try:
        uid = req.session['uid']
    except:
        return redirect('login')

    # verifica se o usuario esta cadastrado, caso nao esteja, pede para ele se cadastrar
    try:
        usuario = Cliente.objects.filter(uid=uid).values('nome')[0]['nome']
    except:
        context = {
            'logado': True,
            'erro': "Crie seu perfil para poder realizar agendamentos"
        }
        return render(req, 'index.html', context)

    # pesquisa e retorna o agendamento do usuario
    context = {
        'elements': Agendamento.objects.filter(cliente_id=uid),
        'usuario': usuario,
        'logado': True
    }
    return render(req, 'meus_agendamentos.html', context)


# retorna pagina com os detalhes do agendamento selecionado pelo usuario
def detalhes(req, pk):
    if req.method == 'GET':
        try:
            uid = req.session['uid']
        except:
            return redirect('login')
        context = {
            'logado': True,
            'agendInfo': Agendamento.objects.filter(id=pk).values()[0],
            'userInfo': Cliente.objects.filter(uid=uid).values()[0]
        }
        return render(req, 'detalhes.html', context)

    # if POST, atualiza o agendamento
    if req.method == 'POST':
        id = pk
        animal = req.POST['animal']
        porte = req.POST['porte']
        tp_tosa = req.POST['tp_tosa']
        observacao = req.POST['observacao']
        data = req.POST['data']
        horario = req.POST['horario']
        dt_agendamento = datetime.datetime.now()
        uid = Cliente.objects.get(uid=req.session['uid'])

        agendamento = Agendamento(id=id, animal=animal, porte=porte, tp_tosa=tp_tosa,
            observacao=observacao, data=data, horario=horario,
            dt_agendamento=dt_agendamento, cliente=uid)

        agendamento.save()
        return redirect('meus_agendamentos')


# deleta o agendamento selecionado
def deletar(req, pk):
    if req.method == 'GET':
        try:
            uid = req.session['uid']
        except:
            return redirect('login')
        context = {
            'logado': True,
            'agendInfo': Agendamento.objects.filter(id=pk).values()[0],
        }
        return render(req, 'deletar.html', context)

    if req.method == 'POST':
        agend = Agendamento.objects.filter(id=pk)
        agend.delete()
        return redirect('meus_agendamentos')


# retorna a tela para realizar o agendamento no site se for GET, 
## tenta salvar o agendamento no banco se for POST
def agendar(req):
    if req.method == 'GET':
        try:
            uid = req.session['uid']
        except:
            return redirect('login')

        try:
            usuario = Cliente.objects.filter(uid=uid).values('nome')[0]['nome']
        except:
            context = {
                'logado': True,
                'erro': "Crie seu perfil para poder realizar agendamentos"
            }
            return render(req, 'index.html', context)

        context = {'logado': True}
        return render(req, 'agendamento.html', context)

    if req.method == 'POST':
        uid = req.session['uid']

        try:
            agendamento = Agendamento.objects.filter(cliente_id=uid).defer('dt_agendamento').values()[0]
        except:
            agendamento = None

        if agendamento is None:
            animal = req.POST['animal']
            porte = req.POST['porte']
            tp_tosa = req.POST['tp_tosa']
            observacao = req.POST['observacao']
            data = req.POST['data']
            horario = req.POST['horario']
            dt_agendamento = datetime.datetime.now()
            uid = Cliente.objects.get(uid=uid)

            agendamento = Agendamento(animal=animal, porte=porte, tp_tosa=tp_tosa,
                observacao=observacao, data=data, horario=horario,
                dt_agendamento=dt_agendamento, cliente=uid)

            agendamento.save()
            return redirect('meus_agendamentos')

        else:
            context = {
                'logado': True,
                'erro': True
            }
            return render(req, 'agendamento.html', context)


# api para realizar o agendamento
def apiAgendar(req):
    if req.method == 'POST':
        # recebe a requisicao em bytes e decodifica em string
        data_bytes = req.body.decode('utf8').replace("'", '"')

        # converte a string em dicionario para obter os dados
        data_dict = json.loads(data_bytes)

        uid = data_dict['uid']

        try:
            agendamento = Agendamento.objects.filter(cliente_id=uid).defer('dt_agendamento').values()[0]
        except:
            agendamento = None

        if agendamento is None:
            try:
                animal = data_dict['animal']
                porte = data_dict['porte']
                tp_tosa = data_dict['tp_tosa']
                observacao = data_dict['observacao']
                data = data_dict['data']
                horario = data_dict['horario']
                dt_agendamento = datetime.datetime.now()
            except:
                retorno = []
                retorno.append({"status": "403", "msg": "informacoes invalidas"})

                return HttpResponse(json.dumps(retorno))

            # tenta salvar dados do banco
            try:
                agendamento = Agendamento(animal=animal, porte=porte, tp_tosa=tp_tosa,
                    observacao=observacao, data=data, horario=horario,
                    dt_agendamento=dt_agendamento, cliente_id=uid)

                agendamento.save()
            except Exception as e:
                print(e)
                retorno = []
                retorno.append({"status": "403", "msg": "erro no agendamento"})

                return HttpResponse(json.dumps(retorno))

            # retorno
            retorno = []
            retorno.append({"status": "201", "msg": "agendado"})
            return HttpResponse(json.dumps(retorno))
        else:
            retorno = []
            retorno.append({"status": "403", "msg": "cliente ja tem um agendamento registrado"})
            return HttpResponse(json.dumps(retorno))


# api que retorna os agendamentos do usuario
def apiMeusAgendamentos(req):
    if req.method == "GET":
        uid = req.GET.get("uid")

        if uid is None:
            erro = {"status": "403", "msg": "uid invalido"}
            retorno = []
            retorno.append(erro)
            return HttpResponse(json.dumps(retorno))
        else:
            try:
                agendamento = Agendamento.objects.filter(cliente_id=uid).defer('dt_agendamento').values()[0]
            except:
                erro = {"status": "403", "msg": "sem agendamentos"}
                retorno = []
                retorno.append(erro)
                return HttpResponse(json.dumps(retorno))

            retorno = []
            retorno.append(agendamento)

            # retorna os dados do usuario como array
            return HttpResponse(json.dumps(retorno))

    if req.method == "POST":
        # recebe a requisicao em bytes e decodifica em string
        data_bytes = req.body.decode('utf8').replace("'", '"')

        # converte a string em dicionario para obter os dados
        data_dict = json.loads(data_bytes)

        # tenta pegar uid enviado na requisicao
        try:
            uid = data_dict["uid"]
        except:
            erro = {"status": "403", "msg": "uid invalido"}
            retorno = []
            retorno.append(erro)
            return HttpResponse(json.dumps(retorno))

        # tenta consultar no banco os dados do cliente correspondentes ao uid
        try:
            agendamento = Agendamento.objects.filter(cliente_id=uid).defer('dt_agendamento').values()[0]
        except:
            erro = {"status": "403", "msg": "sem agendamentos"}
            retorno = []
            retorno.append(erro)
            return HttpResponse(json.dumps(retorno))

        # retorna os dados do usuario como array
        retorno = []
        retorno.append(agendamento)

        return HttpResponse(json.dumps(retorno))


# Retorna os dados do usuario
def apiPerfil(req):
    if req.method == "GET":
        uid = req.GET.get("uid")
        if uid is None:
            erro = {"status": "403", "msg": "uid invalido"}
            retorno = []
            retorno.append(erro)
            return HttpResponse(json.dumps(retorno))
        else:
            try:
                usuario = Cliente.objects.filter(uid=uid).values()[0]
            except:
                erro = {"status": "403", "msg": "usuario inexistente"}
                retorno = []
                retorno.append(erro)
                return HttpResponse(json.dumps(retorno))

            retorno = []
            retorno.append(usuario)

            # retorna os dados do usuario como array
            return HttpResponse(json.dumps(retorno))

    if req.method == "POST":
        # recebe a requisicao em bytes e decodifica em string
        data_bytes = req.body.decode('utf8').replace("'", '"')

        # converte a string em dicionario para obter os dados
        data_dict = json.loads(data_bytes)

        # tenta pegar uid enviado na requisicao
        try:
            uid = data_dict["uid"]
        except:
            erro = {"status": "403", "msg": "uid invalido"}
            retorno = []
            retorno.append(erro)
            return HttpResponse(json.dumps(retorno))

        # tenta consultar no banco os dados do cliente correspondentes ao uid
        try:
            usuario = Cliente.objects.filter(uid=uid).values()[0]
        except:
            erro = {"status": "403", "msg": "uid invalido"}
            retorno = []
            retorno.append(erro)
            return HttpResponse(json.dumps(retorno))

        # criando array para retornar
        retorno = []
        retorno.append(usuario)

        # retorna os dados do usuario como array
        return HttpResponse(json.dumps(retorno))

        # retorna os dados do usuario com JsonResponse
        # return JsonResponse(agendamento)


# api para deletar um agendamento
def api_deletar(req):
    # recebe a requisicao em bytes e decodifica em string
    data_bytes = req.body.decode('utf8').replace("'", '"')

    # converte a string em dicionario para obter os dados
    data_dict = json.loads(data_bytes)

    try:
        agend = Agendamento.objects.filter(id=data_dict['id'])
    except:
        retorno = [{'status': '403', 'msg': 'agendamento nao encontrado'}]
        return HttpResponse(json.dumps(retorno))

    try:
        agend.delete()
    except:
        retorno = [{'status':'403', 'msg': 'erro ao excluir agendamento'}]
        return HttpResponse(json.dumps(retorno))

    retorno = [{'status':'200', 'msg': 'agendamento excluido'}]
    return HttpResponse(json.dumps(retorno))
