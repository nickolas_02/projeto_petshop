from django.contrib import admin
from .models import Agendamento, Cliente

# Register your models here.
admin.site.register(Agendamento)
admin.site.register(Cliente)
