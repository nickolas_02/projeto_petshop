# from django.contrib import admin
from django.urls import path

from .views import homePageView, agendar, apiAgendar, login, detalhes
from .views import registrar, logout, agendamentos, quemSomos
from .views import deletar, meuPerfil, cadastro, apiCadastro
from .views import apiPerfil, apiMeusAgendamentos, api_deletar

urlpatterns = [
    path('', homePageView, name='index'),
    path('agendamento/', agendar, name='agendamento'),
    path('meus_agendamentos', agendamentos, name='meus_agendamentos'),
    path('api_agendar/', apiAgendar, name='api_agendamento'),
    path('login/', login, name='login'),
    path('logout/', logout, name='logout'),
    path('registrar/', registrar, name='registrar'),
    path('cadastro/', cadastro, name='cadastro'),
    path('meu_perfil/', meuPerfil, name='meu_perfil'),
    path('detalhes/<int:pk>/', detalhes, name='detalhes'),
    path('deletar/<int:pk>/', deletar, name='deletar'),
    path('api_cadastro/', apiCadastro, name='api_cadastro'),
    path('quem_somos', quemSomos, name='quemSomos'),
    path('api_perfil/', apiPerfil, name="api_perfil"),
    path('api_agendamentos/', apiMeusAgendamentos, name="api_meus_agendamentos"),
    path('api_deletar/', api_deletar, name='api_deletar')
]
