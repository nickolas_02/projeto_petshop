from django.db import models

# Create your models here.


class Cliente(models.Model):
    nome = models.CharField(max_length=100)
    telefone = models.CharField(max_length=15)
    dt_nascimento = models.CharField(max_length=10)
    cpf = models.CharField(max_length=15)
    sexo = models.CharField(max_length=10)
    rua = models.CharField(max_length=100)
    numero = models.CharField(max_length=10)
    complemento = models.CharField(max_length=100, blank=True)
    bairro = models.CharField(max_length=100)
    cidade = models.CharField(max_length=100)
    cep = models.CharField(max_length=10, default="00000-000")
    uid = models.CharField(max_length=50, primary_key=True, unique=True, default="User Id")

    def __str__(self):
        return self.nome + ' - ' + self.cpf


class Agendamento(models.Model):
    animal = models.CharField(max_length=50)
    porte = models.CharField(max_length=50)
    tp_tosa = models.CharField(max_length=50)
    observacao = models.CharField(max_length=100, blank=True)
    data = models.CharField(max_length=10)
    horario = models.CharField(max_length=5)
    dt_agendamento = models.CharField(max_length=50)
    cliente = models.ForeignKey(Cliente, on_delete=models.CASCADE)

    def __str__(self):
        return str(self.data) + ' - ' + self.animal
