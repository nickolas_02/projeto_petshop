from django import forms
from .models import Agendamento


class PostForm(forms.ModelForm):
    class Meta:
        model = Agendamento
        exclude = []
