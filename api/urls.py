from django.urls import path, include

from .views import AgendamentoViewSet

from rest_framework import routers

rounter = routers.DefaultRouter()
rounter.register(r'', AgendamentoViewSet)

urlpatterns = [
    # path('', AgendamentoAPIView),
    path('', include(rounter.urls)),
]
