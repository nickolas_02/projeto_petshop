from rest_framework import serializers

from petshop.models import Agendamento


class AgendamentoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Agendamento
        exclude = []
        # fields = ('nome_dono', 'endereco', 'telefone', 'nome_animal',
        #  'porte', 'tipo_tosa', 'observacao', 'data', 'horario')
