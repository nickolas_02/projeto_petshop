from django.shortcuts import render

from rest_framework import viewsets

from petshop.models import Agendamento
from .serializers import AgendamentoSerializer

# Create your views here.


class AgendamentoViewSet(viewsets.ModelViewSet):
    queryset = Agendamento.objects.all()
    serializer_class = AgendamentoSerializer
