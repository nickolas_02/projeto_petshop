$(document).ready(function(){
    $('#cpf').mask('000.000.000-00')
    $('#rg').mask('00.000.000-0')
    $('#cep').mask('00000-000')
    $('#dt_nascimento').mask('00/00/0000')
    
    //Máscara para campo do telefone
    var behavior = function (val) {
        return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009'
    },
    options = {
        onKeyPress: function (val, e, field, options) {
            field.mask(behavior.apply({}, arguments), options)
        }
    }
    $('#telefone').mask(behavior, options)


     //********** CONSULTAR API DO CEP E RETORNAR ENDEREÇO *********//
    function limpa_formulário_cep() {
        // Limpa valores do formulário de cep.
        $("#rua").val("")
        $("#bairro").val("")
        $("#cidade").val("")
    }

    //Quando o campo cep perde o foco.
    $("#cep").blur(function() {

        //Nova variável "cep" somente com dígitos.
        var cep = $(this).val().replace(/\D/g, '')

        //Verifica se campo cep possui valor informado.
        if (cep != "") {

            //Expressão regular para validar o CEP.
            var validacep = /^[0-9]{8}$/

            //Valida o formato do CEP.
            if(validacep.test(cep)) {

                //Preenche os campos com "..." enquanto consulta webservice.
                $("#rua").val("...")
                $("#bairro").val("...")
                $("#cidade").val("...")

                //Consulta o webservice viacep.com.br/
                $.getJSON("https://viacep.com.br/ws/"+ cep +"/json/?callback=?", function(dados) {

                    if (!("erro" in dados)) {
                        //Atualiza os campos com os valores da consulta.
                        $("#rua").val(dados.logradouro)
                        $("#bairro").val(dados.bairro)
                        $("#cidade").val(dados.localidade)
                    } //end if.
                    else {
                        //CEP pesquisado não foi encontrado.
                        limpa_formulário_cep();
                        alert("CEP não encontrado.")
                    }
                })
            }
            else {
                //cep é inválido.
                limpa_formulário_cep();
                alert("Formato de CEP inválido.")
            }
        }
        else {
            //cep sem valor, limpa formulário.
            limpa_formulário_cep()
        }
    })
})



///// validar cpf
var cpfValidation = false
$('#cpf').blur(function(){
    var strCPF = $('#cpf').val().replace(/\D/g, '')
    var soma
    var resto
    soma = 0
    if (
        (strCPF == "00000000000") ||
        (strCPF == "11111111111") ||
        (strCPF == "22222222222") ||
        (strCPF == "33333333333") ||
        (strCPF == "44444444444") ||
        (strCPF == "55555555555") ||
        (strCPF == "66666666666") ||
        (strCPF == "77777777777") ||
        (strCPF == "88888888888") ||
        (strCPF == "99999999999")
    ){
        alert("Cpf inválido")
        cpfValidation = false
        return false
    } 
    for (i=1; i<=9; i++){
        soma = soma + parseInt(strCPF.substring(i-1, i)) * (11 - i)
    } 
    resto = (soma * 10) % 11
    
    if ((resto == 10) || (resto == 11)){
        resto = 0
    }
    if (resto != parseInt(strCPF.substring(9, 10))){
        alert("Cpf inválido")
        cpfValidation = false
        return false
    }
    soma = 0;
    for (i = 1; i <= 10; i++){
        soma = soma + parseInt(strCPF.substring(i-1, i)) * (12 - i)
    }   
    resto = (soma * 10) % 11
   
    if ((resto == 10) || (resto == 11)){
        resto = 0
    }
    if (resto != parseInt(strCPF.substring(10, 11))){
        alert("Cpf inválido")
        cpfValidation = false
        return false
    }
    cpfValidation = true
    return true
})

// verifica se o cpf  e idade sao validos ao enviar o cadastro, caso false, nao envia o formulario
function validateForm(){
    if(cpfValidation == false){
        alert("Cpf inválido")
        cpfValidation = false
        return false
    }
    var dt_nascimento = $('#dt_nascimento').val()
    var ano = dt_nascimento.substring(6)
    var ano_atual = new Date().getFullYear()
    
    var idade = ano_atual - ano

    if(idade < 16){
        alert("Cliente menor de 16 anos")
        return false

    }

    return true
}
